<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\admin\controllers;

use yii\web\Controller;
use app\models\NewsForm;
use Yii;
/**
 * Description of NewsController
 *
 * @author kas
 */
class NewsController extends Controller {

    //put your code here

    public function actionIndex() {
        $newsArray = \app\models\News::getNewsForAdmin();
        return $this->render('index',['newsArray'=>$newsArray]);
    }

    public function actionCreate() {
        $model = new NewsForm();
//        $model->setScenario('create');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->handler();
            return $this->redirect(['index']);
        } else {
            return $this->render('form', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
         $model = new NewsForm(['id'=>$id]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->handler();
            return $this->redirect(['index']);
        } else {
            return $this->render('form', [
                        'model' => $model,
            ]);
        }
    }

}

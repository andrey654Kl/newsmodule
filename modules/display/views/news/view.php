<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = $news['title'];
$this->params['breadcrumbs'][] = ['label' => 'Список новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="news_page">
    <h1><?= $this->title ?></h1>
    <h2>Тематика новости : <?=$news['theme_title'] ?></h2>
    <div>Дата Публикации : <?=date('d.m.Y ',strtotime($news['date']))?></div>
    <div id="news_content" style="padding: 20px"><?=$news['text']?></div>
</div>
<div><?=  Html::a('Список всех новостей',['index'])?></div>
<?php

use yii\helpers\Html;

$this->title = 'Список новостей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p class="create-btn-cont">
<?= Html::a('Добавить Новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <table class="table">
        <tr>
            <th>id новости</th>
            <th>Название новости</th>
            <th>Тема новости</th>
            <th>Дата публикации новости</th>
            <th></th>
        </tr>
<?php foreach ((array) $newsArray as $news): ?>
            <tr>
                <th><?=$news['news_id']?></th>
                <th><?=$news['title']?></th>
                <th><?=$news['theme_title']?></th>
                <th><?=date('d.m.Y',strtotime($news['date']))?></th>
                <th><?=  Html::a('Редактировать',['update','id'=>$news['news_id']])?></th>
            </tr>
<?php endforeach ?>
    </table>

</div>

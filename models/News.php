<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use app\components\DbQuery;

/**
 * Description of News
 *
 * @author kas
 */
class News {

    //put your code here
    const TABLE_NAME = 'news'; // Название таблицы новостей
    const MAX_COUNT_ON_PAGE = 5; // Максимальное количество новостей на странице

    /**
     * Выбоока списка новостей для административной части
     * @return type
     */

    static function getNewsForAdmin() {
        $query = 'select n.news_id,n.title,n.date,t.theme_title from ' . self::TABLE_NAME . ' as n ';
        $query .= 'join themes as t on t.theme_id = n.theme_id order by n.date DESC';
        $newsArray = DbQuery::select($query, true);
        return $newsArray;
    }

    /**
     * Возврат новости по ее идентификатору
     * @param type $id
     * @return type
     */
    static function getNewsByID($id) {
        return DbQuery::select('select news_id,title,theme_id,date,text from news where news_id = ' . $id . ' limit 1');
    }

    /**
     * Возврат новости для страницы просмотра 
     * @param type $id
     * @return type
     */
    static function getNewsForView($id) {
        $query = 'select n.news_id,n.title,n.date,t.theme_title,n.text from ' . self::TABLE_NAME . ' as n ';
        $query .= 'join themes as t on t.theme_id = n.theme_id ';
        $query .= 'where n.news_id = ' . $id . ' limit 1';
        $news = DbQuery::select($query);
        return $news;
    }

    /**
     * Возврат массива новостей для главной страницы вывода новостей
     * @param type $where Условия для новостей
     * @param type $page текущая страница
     * @return type
     */
    static function getNewsForMainDisplay($where, $page = 1) {
        $query = 'select n.news_id,n.title,n.date,t.theme_title,n.text from ' . self::TABLE_NAME . ' as n ';
        $query .= 'join themes as t on t.theme_id = n.theme_id ';
        $query.=$where;
        $query.=' order by n.date DESC ';
        $query .= 'limit ' . (($page - 1) * self::MAX_COUNT_ON_PAGE) . ',' . self::MAX_COUNT_ON_PAGE;
        $news = DbQuery::select($query, true);
        return $news;
    }

    /**
     * Возвращает массив с данными необходимыми для вывода списка новостей на экран(Сами новости, общее число новостей и т.д.)
     * @param type $theme_id Идентификотор темы
     * @param type $year Выбранный год
     * @param type $month Выбранный месяц
     * @param type $page Текущая страница
     * @return type
     */
    static function getDataForMainPage($theme_id = false, $year = false, $month = false, $page = 1) {
        $where = '';
        if ($theme_id) {
            $where = 'where n.theme_id = ' . $theme_id;
        } elseif ($year && !$month) {
            $where = 'where year(n.date) = ' . $year;
        } elseif ($year && $month) {
            $where = 'where year(n.date) = ' . $year . ' AND month(n.date) = ' . $month;
        }
        $count = self::getSummaryCount($where);
        $data['totalcount'] = $count;
        $data['countonpage'] = self::MAX_COUNT_ON_PAGE;
        $data['totalpages'] = ceil($count / self::MAX_COUNT_ON_PAGE);
        $data['currentpage'] = $page;
        $data['news'] = self::getNewsForMainDisplay($where, $page);
        return $data;
    }

    /**
     * Возвращает общее количество новостей, удовлетворяюших условию $where
     * @param type $where
     * @return type
     */
    static function getSummaryCount($where) {
        $query = 'select count(n.news_id) as count from ' . self::TABLE_NAME . ' as n ';
        $query .= 'join themes as t on t.theme_id = n.theme_id ';
        $query.=$where;
        $count = DbQuery::select($query);
        return $count['count'];
    }

    /**
     * Возврат массива, необходимого для вывода левого меню
     * @return type
     */
    static function getArrayForLeftMenu() {
        $array['years'] = self::getCountNewsByDate();
        $array['themes'] = self::getCountNewsByThemes();
        return $array;
    }

    /**
     * Группировка количества новостей по каждой теме
     * @return type
     */
    static function getCountNewsByThemes() {
        $query = 'select t.theme_id as id,t.theme_title as title, count(n.news_id) as count from themes as t ';
        $query .= 'left join ' . self::TABLE_NAME . ' as n on t.theme_id = n.theme_id ';
        $query .= 'group by t.theme_id';
        $res = DbQuery::select($query);
        return $res;
    }

    /**
     * Возвращает количество новостей сгруппированых по году и месяцу
     * @return boolean
     */
    static function getCountNewsByDate() {
        $query = "SELECT YEAR(date) as year, MONTH(date) as month, count(distinct news_id) as count from news group by year,month order by year,month";
        $res = DbQuery::select($query);
        if ($res) {
            return self::formatDateArray($res);
        }
        return false;
    }

    /**
     * Приведение массива из запроса к формату [год1 => [[месяц1]=>количество статей в месяце1 в году 1]
     * @param type $dateArray
     * @return type
     */
    static function formatDateArray($dateArray) {
        $result = [];
        foreach ((array) $dateArray as $row) {
            $result[$row['year']][$row['month']] = $row['count'];
        }
        return $result;
    }

}

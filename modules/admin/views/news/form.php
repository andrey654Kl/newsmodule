<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = 'Создать новость';
$this->params['breadcrumbs'][] = ['label' => 'Список новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs('$("#newsform-date").datepicker({dateFormat: "dd.mm.yy"});');
$this->registerCssFile('http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
$this->registerJsFile('https://code.jquery.com/ui/1.12.1/jquery-ui.js');
?>
<div class="news-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="news-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'date')->textInput() ?>

        <?= $form->field($model, 'theme_id')->dropDownList(\app\models\NewsForm::getThemeIds(),['prompt'=>'Выберите тему']) ?>

        <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>


        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
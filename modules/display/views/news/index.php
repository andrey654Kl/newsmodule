<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = 'Список Новостей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="content" style="height:100%">
    <div id="left_block" style="width: 200px;float: left;min-height: 100%;position: fixed">
        <?= $this->render('_leftmenu', ['menu' => $leftMenu]) ?>
    </div>
    <div id="right_block" style="width: 800px;margin-left: 210px;">
        <div id="list_news">
            <?php if ($newsArray['news']): ?>
                <?php foreach ((array) $newsArray['news'] as $news): ?>
                    <?= $this->render('_news', ['news' => $news]) ?>
                <?php endforeach; ?>
            <?php else: ?>
                В данной категории новости отсутствуют
            <?php endif ?>
            <?php if ($newsArray['totalpages'] > 1): ?>    
                <div id="pager">
                    <ul class="pagination">
                        <?php for ($i = 1; $i <= $newsArray['totalpages']; $i++): ?>
                        <li <?= ($i == $newsArray['currentpage'] ? 'class="active"' : '') ?>><?=  Html::a($i,Url::current(['page' => $i]))?></li>
                        <?php endfor; ?>
                    </ul>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>

<div class="news_place" style="padding: 30px">
    <div class="news_header" style="font-size: 150%"><?=$news['title']?></div>
    <div class="news_theme">Тема : <?=$news['theme_title']?></div>
    <div class="news_date_public">Дата публикации : <?=date('d.m.Y',strtotime($news['date']))?></div>
    <div class="news_text_place">
        <?= (mb_strlen($news['text']) < 256 ? $news['text'] : mb_substr ($news['text'], 0,256)."..."); ?>
    </div>
    <span><?=  yii\helpers\Html::a('Читать далее',['view','id'=>$news['news_id']])?></span>
</div>

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use app\components\DbQuery;
use app\models\News;

/**
 * Description of NewsForm
 *
 * @author kas
 */
class NewsForm extends \yii\base\Model {

    //put your code here
    public $id;
    public $title;
    public $theme_id;
    public $date;
    public $text;

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'theme_id' => 'Тема',
            'date' => 'Дата публикации',
            'text' => 'Текст'
        ];
    }

    public function __construct($config = array()) {
        parent::__construct($config);
        if (isset($config['id'])) {
            $news = News::getNewsByID($config['id']);
            if ($news) {
                $this->setAttr($news);
            }
        }
    }

    public function rules() {
        return [
            [['title', 'theme_id', 'date', 'text'], 'required'],
            [['id'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * обработка формы
     * @return boolean
     */
    function handler() {
        if ($this->validate()) {
            if ($this->id) {
                $query = $this->getUpdateQuery();
            } else {
                $query = $this->getInsertQuery();
            }
            $res = DbQuery::execute($query);
            return $res;
        }
        return false;
    }

    /**
     * Возвращает массив для выбора тем в форме
     * @return type
     */
    static function getThemeIds() {
        $res = DbQuery::select('select theme_id,theme_title from themes');
        $themeIds = \yii\helpers\ArrayHelper::map($res, 'theme_id', 'theme_title');
        return $themeIds;
    }

    /**
     * Формирует запрос на добавление в базу новой новости
     * @return string
     */
    function getInsertQuery() {
        $data = $this->getDataArray();
        $keys = implode('`,`', array_keys($data));
        $values = implode("','", array_values($data));
        $query = "INSERT INTO " . News::TABLE_NAME . " (`" . $keys . "`) values ('" . $values . "')";
        return $query;
    }

    /**
     * Формирует запрос на редактирование новости
     * @return string
     */
    function getUpdateQuery() {
        $data = $this->getDataArray();
        $columns = [];
        foreach ((array) $data as $key => $field) {
            $columns[] = "`" . $key . "` = '" . $field . "'";
        }
        $query = "Update " . News::TABLE_NAME . " SET " . implode(',', $columns) . " where news_id = " . $this->id;
        return $query;
    }

    /**
     * Возвращает массив в формате Название поля => значение поля, для добавление и изменения новостейF
     * @return type
     */
    function getDataArray() {
        return [
            'title' => $this->title,
            'text' => $this->text,
            'theme_id' => $this->theme_id,
            'date' => date('Y-m-d', strtotime($this->date)),
        ];
    }

    /**
     * Устанавливает свойства формы из массива
     * @param type $data
     */
    function setAttr($data) {
        $this->title = $data['title'];
        $this->theme_id = $data['theme_id'];
        $this->date = date('d.m.Y', strtotime($data['date']));
        $this->text = $data['text'];
    }

}

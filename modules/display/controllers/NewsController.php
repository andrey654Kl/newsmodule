<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\display\controllers;

use yii\web\Controller;
use Yii;
use app\models\News;

/**
 * Description of NewsController
 *
 * @author kas
 */
class NewsController extends Controller {

    //put your code here

    function actionIndex($theme=false,$year=false,$month=false,$page=1) {
        $newsArray = News::getDataForMainPage($theme, $year, $month, $page);
        $leftMenu = News::getArrayForLeftMenu();
        return $this->render('index',['newsArray'=>$newsArray,'leftMenu'=>$leftMenu]);
    }

    function actionView($id) {
        $news = News::getNewsForView($id);
        if (!$news) {
            throw new \yii\web\NotFoundHttpException('Данной новости не существует');
        }
        return $this->render('view', ['news' => $news]);
    }

}

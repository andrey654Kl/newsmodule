<?php
use yii\helpers\Html;
$nameMonthsArray = [
    1 => 'Январь',
    2 => 'Февраль',
    3 => 'Март',
    4 => 'Апрель',
    5 => 'Май',
    6 => 'Июнь',
    7 => 'Июль',
    8 => 'Август',
    9 => 'Сентябрь',
    10 => 'Октябрь',
    11 => 'Ноябрь',
    12 => 'Декабрь'
];
?>
<div id="menu">
    <ul><?=Html::a('Все новости',['index'])?></ul>
    <div id="by_date">
        <ul>
            <?php foreach ((array) $menu['years'] as $nameyear => $year): ?>
            <li><?= Html::a($nameyear,['index','year'=>$nameyear]) ?>
                    <ul>
                        <?php foreach ((array) $year as $month => $count): ?>
                        <li><?= Html::a($nameMonthsArray[$month]." ($count)",  ['index','year'=>$nameyear,'month'=>$month])?></li>
                        <?php endforeach ?>
                    </ul>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
    <div id="ny_themes">
        <ul>
             <?php foreach ((array) $menu['themes'] as $theme): ?>
             <li><?= Html::a($theme['title']." (".$theme['count'].")",['index','theme'=>$theme['id']])?></li>
            <?php endforeach?>
        </ul>
    </div>
</div>
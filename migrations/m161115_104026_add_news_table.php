<?php

use yii\db\Migration;

class m161115_104026_add_news_table extends Migration {

    public function up() {
        $this->createTable('news', [
            'news_id' => $this->primaryKey(),
            'date' => $this->timestamp(),
            'theme_id' => $this->integer()->notNull(),
            'text' => $this->text(),
            'title' => $this->string(255)
        ]);
        $this->addForeignKey('fk_theme1', 'news', 'theme_id', 'themes', 'theme_id', 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropForeignKey('fk_theme1', 'news');
        $this->dropTable('news');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}

<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div><?=Html::a('Администрирование новостями',['/admin/news'])?></div>
    <div><?=Html::a('Вывод новостей',['/display/news'])?></div>
</div>
